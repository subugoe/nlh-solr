#FROM solr:8
FROM solr:6.5.1

USER root

ARG sUID
ARG sGID

ENV SOLR_SERVER_BASE        /opt/solr/server
ENV SOLR_HOME               /opt/solr/server/solr

COPY  config/solr.in.sh     /opt/solr/bin/solr.in.sh

# core 1

RUN mkdir -p $SOLR_HOME/mycores/nlh

COPY config/schema.xml           $SOLR_HOME/mycores/nlh/conf/schema.xml
COPY config/solrconfig.xml       $SOLR_HOME/mycores/nlh/conf/solrconfig.xml
COPY config/stopwords.txt        $SOLR_HOME/mycores/nlh/conf/stopwords.txt
COPY config/synonyms.txt         $SOLR_HOME/mycores/nlh/conf/synonyms.txt

COPY config/mapping-ISOLatin1Accent.txt        $SOLR_HOME/mycores/nlh/conf/mapping-ISOLatin1Accent.txt
COPY config/mapping-FoldToASCII.txt            $SOLR_HOME/mycores/nlh/conf/mapping-FoldToASCII.txt
COPY config/core.properties                    $SOLR_HOME/mycores/nlh/core.properties

# core 2

RUN mkdir -p $SOLR_HOME/mycores/nlh_tmp

COPY config/schema2.xml           $SOLR_HOME/mycores/nlh_tmp/conf/schema.xml
COPY config/solrconfig.xml       $SOLR_HOME/mycores/nlh_tmp/conf/solrconfig.xml
COPY config/stopwords.txt        $SOLR_HOME/mycores/nlh_tmp/conf/stopwords.txt
COPY config/synonyms.txt         $SOLR_HOME/mycores/nlh_tmp/conf/synonyms.txt

COPY config/mapping-ISOLatin1Accent.txt        $SOLR_HOME/mycores/nlh_tmp/conf/mapping-ISOLatin1Accent.txt
COPY config/mapping-FoldToASCII.txt            $SOLR_HOME/mycores/nlh_tmp/conf/mapping-FoldToASCII.txt
COPY config/core2.properties                    $SOLR_HOME/mycores/nlh_tmp/core.properties


RUN chown -R $sUID:$sGID $SOLR_SERVER_BASE
USER $sUID:$sGID
