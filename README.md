# Solr for NLH

This is a repository for creating Docker Images for the NLH Solr.

The NLH SOLR will be intergrated in other projects as a git-subproject, it can also deployed standalone. 

# How to use via docker-compose
To persist the data you have to use a data volume. In the following example the image will be pulled from the gitlab docker registry. You can access solr via hostIP:8122 or via docker network solr:8983.

    version: '2'
    services:

        solr:
            image: docker.gitlab.gwdg.de/subugoe/nlh-solr:live
            ports:
                - '8122:8983'
            volumes:
            - /path/to/index/in/hostsystem:/opt/solr/server/solr/mycores/nlh
            environment:
                - JVM_OPTS=-Xmx6g -Xms6g -XX:MaxPermSize=1024m
    

# Image description
The image build is defined in the Dockerfile. It creates two cores, the first 'nlh' uses the schema.xml, the second 'nlh_tmp' uses the schema2.xml. The first reflects the currently used schema(in dev and live), schema2 is the changed schema, which reflects changes e.g. to reduce the index size and add normalized fields (facetes).

# Startup 
Can be done via docker-compose tools:

        #!/bin/bash
        docker-compose pull && \
        docker-compose down && \
        docker-compose up -d --build

# Differnces between schema and schema2:
The schema2 add fields:
* title_page
* title_original
* facet_normalized_genre
* facet_normalized_place_term
* periodical_name
* navi_date

